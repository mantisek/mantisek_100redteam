import socket


def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('0.0.0.0', 45000))

    s.listen(1)
    conn, addr = s.accept()
    connection_string = f"You have connected as {addr}:"
    conn.send(connection_string.encode('utf-8'))

    while True:
        data = conn.recv(1024)
        if not data:
            break
        if data == "quit":
            break
        conn.sendall(data)


    conn.close()




if __name__ == "__main__":
    main()